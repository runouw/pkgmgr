package animals.barn;

import animals.Animal;

public class Sheep extends Animal {
    @Override
    public String say() {
        return "Beep";
    }
}

