package animals.house;

import animals.Animal;

public class Dog extends Animal {
    @Override
    public String say() {
        return "Woof";
    }
}
