/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.pkgmgr;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 * A utility class designed for constructing new instances of RPackageManager.
 * @author zmichaels
 */
public final class RPackageManagerFactory {   
    private RPackageManagerFactory() {}

    /**
     * Constructs a new instance of RPackageManager.
     *
     * @param cacheDir the directory to cache all compiled packages.
     * @param srcDirs the directories to scan for packages.
     * @return the RPackageManager.
     */
    public static RPackageManager newPackageManager(final Path cacheDir, final Path... srcDirs) {
        return newPackageManager(cacheDir, Arrays.asList(srcDirs));
    }
    
    /**
     * Constructs a new instance of RPackageManager.
     *
     * @param cacheDir the directory to cache all compiled packages.
     * @param srcDirs the directories to scan for packages.
     * @return the RPackageManager.
     */
    public static RPackageManager newPackageManager(final Path cacheDir, final List<Path> srcDirs) {
        Objects.requireNonNull(cacheDir, "Package cache directory must not be null!");
        Objects.requireNonNull(srcDirs, "Package source directory must not be null!");

        return new RPackageManager(cacheDir, srcDirs);
    }

    /**
     * Creates a new instance of RPackageManager using the system's temporary
     * directory for cached packages. This cached data cannot be reused across
     * sessions.
     *
     * @param srcDirs the source directories.
     * @return the RPackageManager.
     * @throws IOException if the temporary directory cannot be created.
     */
    public static RPackageManager newNoCachePackageManager(final List<Path> srcDirs) throws IOException {
        Objects.requireNonNull(srcDirs, "A source directory must be defined when creating a no-cache package manager!");

        final Path cacheDir = Files.createTempDirectory("com.runouw.pkgmgr");

        return new RPackageManager(cacheDir, srcDirs);
    }
    
    /**
     * Creates a new instance of RPackageManager using the system's temporary
     * directory for cached packages. This cached data cannot be reused across
     * sessions.
     *
     * @param srcDirs the source directories.
     * @return the RPackageManager.
     * @throws IOException if the temporary directory cannot be created.
     */
    public static RPackageManager newNoCachePackageManager(final Path... srcDirs) throws IOException {
        return newNoCachePackageManager(Arrays.asList(srcDirs));
    }

    /**
     * Creates a new instance of RPackageManager using only pre-compiled
     * packages. The resulting package manager is unable to support source
     * packages. The intended use is for projects that do not need user-extended
     * plugins.
     *
     * @param cacheDir the cache directory.
     * @return the RPackageManager.
     */
    public static RPackageManager newCompiledPackageManager(final Path cacheDir) {
        return new RPackageManager(cacheDir, null);
    }
}
