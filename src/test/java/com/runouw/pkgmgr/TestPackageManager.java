/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.pkgmgr;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author zmichaels
 */
public class TestPackageManager {
    
    static {
        System.setProperty("org.slf4j.simpleLogger.defaultLogLevel", "trace");
    }
    
    private static final Logger LOGGER = LoggerFactory.getLogger(TestPackageManager.class);
    
    @Test
    public void testPackageManager() throws Throwable {
        final Path cacheDir = Paths.get(".pluginCache");
        final Path pluginDir = Paths.get("plugins");
        final RPackageManager pkgmgr = RPackageManagerFactory.newPackageManager(cacheDir, pluginDir);
        
        boolean needsRecompile;
        
        try {
            pkgmgr.scan();
            needsRecompile = pkgmgr.needsRecompile();
        } catch (IOException ex) {
            LOGGER.debug("Scan failed, forcing recompile!");
            needsRecompile = true;
        }
        
        //needsRecompile = true;
        
        if (needsRecompile) {
            LOGGER.info("Recompiling...");
            
            pkgmgr.clear().get();
            pkgmgr.compile();
        }
        
        LOGGER.info("Packages: {}", pkgmgr.getPackageNameSet());
                
        pkgmgr.packages().forEach(pkg -> {
            final String name = pkg.getFullName();
            
            if (pkg.hasExports()) {
                LOGGER.info("Package [{}] exports: {}", name, pkg.getPluginNameSet());
                
                pkg.entries().forEach(entry -> {
                    final String alias = entry.getKey();
                    final Class<?> plugin = entry.getValue();
                    
                    try {
                        LOGGER.info("Plugin: [{}] : {}", alias, plugin.newInstance());
                    } catch (InstantiationException | IllegalAccessException ex) {
                        LOGGER.error(ex.getMessage(), ex);
                    } 
                });                
            }
        });        
    }
}
