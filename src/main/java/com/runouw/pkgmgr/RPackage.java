/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.pkgmgr;

import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A dynamically loaded package managed by RPackageManager. A package may define
 * source files, binaries, and/or resources.
 *
 * @author zmichaels
 */
public final class RPackage {

    private static final Logger LOGGER = LoggerFactory.getLogger(RPackage.class);
    private final RPackageData data;

    RPackage(final RPackageData data) {
        this.data = Objects.requireNonNull(data, "PackageData cannot be null!");
    }

    /**
     * Retrieves the number of plugins defined by the package.
     *
     * @return the number of plugins.
     */
    public int size() {
        return this.data.getExportMap().size();
    }

    /**
     * Retrieves a Stream of dependency packages. May be empty.
     *
     * @return the stream of dependency packages.
     */
    public Stream<RPackage> dependencies() {
        return this.data.getDependentPackages().stream()
                .map(RPackage::new);
    }

    /**
     * Retrieves the vendor of the package.
     *
     * @return the vendor.
     */
    public String getVendor() {
        return this.data.getVendor();
    }

    /**
     * Retrieves the name of the package.
     *
     * @return the name.
     */
    public String getName() {
        return this.data.getName();
    }

    /**
     * Retrieves the version of the package.
     *
     * @return the version.
     */
    public String getVersion() {
        return this.data.getVersion();
    }

    /**
     * Retrieves the simple name of the package. This is defined as
     * [code]vendor:name[/code]. It is possible for a package manager to hold
     * multiple packages with the same simple name.
     *
     * @return the simple name.
     */
    public String getSimpleName() {
        return this.data.getSimpleName();
    }

    /**
     * Retrieves the full name of the package. This is defined as
     * [code]vendor:name:version[/code]. The full name is guaranteed unique in
     * the package manager. It is however possible that there are multiple
     * definitions of the same full name across multiple package managers.
     *
     * @return the full name.
     */
    public String getFullName() {
        return this.data.getFullName();
    }

    /**
     * Retrieves any description of the package (if present). Description is
     * meant to be used as an optional metadata field. Not all packages are
     * required to define this.
     *
     * @return the description. May return empty.
     */
    public Optional<String> getDescription() {
        return this.data.getDescription();
    }

    /**
     * Attempts to select a plugin based on its name.
     *
     * @param alias the plugin name.
     * @return the plugin entry. May return empty if no plugin is exported as
     * the given alias or if the exported class does not exist.
     */
    public Optional<Entry> forName(final String alias) {
        return this.data.getExport(alias)
                .map(className -> new Entry(alias, className));
    }

    /**
     * An entry in the package. Links a plugin name to a plugin class.
     */
    public final class Entry implements Map.Entry<String, Class<?>> {

        private final String alias;
        private final String className;
        private Class<?> plugin;

        private Entry(final String alias, final String className) {
            this.alias = alias;
            this.className = className;
        }

        private Entry(final Map.Entry<String, String> exportInfo) {
            this(exportInfo.getKey(), exportInfo.getValue());
        }

        /**
         * Checks if the plugin is of the specified base class.
         *
         * @param baseClass the base class.
         * @return true if the plugin can be assigned to the base class.
         */
        public boolean isType(final Class<?> baseClass) {
            final Class<?> pluginDef = this.getValue();

            if (pluginDef == null) {
                return false;
            } else {
                return baseClass.isAssignableFrom(pluginDef);
            }
        }

        /**
         * Attempts to get the plugin as the same type as the base class.
         *
         * @param <T> the plugin type.
         * @param baseClass the base class.
         * @return the plugin definition as the base class.
         */
        public <T> Optional<Class<T>> asType(final Class<T> baseClass) {
            final Class<?> pluginDef = this.getValue();

            if (pluginDef == null) {
                return Optional.empty();
            } else if (baseClass.isAssignableFrom(pluginDef)) {
                return Optional.of((Class<T>) pluginDef);
            } else {
                return Optional.empty();
            }
        }

        @Override
        public String getKey() {
            return this.alias;
        }

        @Override
        public Class<?> getValue() {
            if (this.plugin == null) {
                try {
                    this.plugin = Class.forName(this.className, true, RPackage.this.data.getClassLoader());
                } catch (ClassNotFoundException ex) {
                    LOGGER.warn("Could not find export: [{}] in package: [{}]!", this.className, RPackage.this.data.getFullName());
                    LOGGER.debug(ex.getMessage(), ex);
                }
            }

            return this.plugin;
        }

        @Override
        public Class<?> setValue(Class<?> v) {
            throw new UnsupportedOperationException("Replacing a plugin export is not supported!");
        }

    }

    /**
     * Retrieves a Stream of all plugin entries. An Entry is defined as a
     * mapping of plugin name to plugin class.
     *
     * @return the stream of entries.
     */
    public Stream<Entry> entries() {
        return this.data.exports().map(Entry::new);
    }

    /**
     * Retrieves a Set of plugin names.
     *
     * @return the set of plugin names
     */
    public Set<String> getPluginNameSet() {
        return this.data.getExportedAliases();
    }

    @Override
    public boolean equals(final Object o) {
        if (o == null) {
            return false;
        } else if (o == this) {
            return true;
        } else if (o instanceof RPackage) {
            return this.data == ((RPackage) o).data;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return this.data.hashCode();
    }

    @Override
    public String toString() {
        return this.data.getFullName();
    }

    /**
     * Checks if the package defines any plugins.
     *
     * @return true if the package defines exports.
     */
    public boolean hasExports() {
        return !this.data.getExportMap().isEmpty();
    }

    /**
     * Retrieves all of the plugin entries that match the specified plugin base.
     *
     * @param <ClassT> the base type.
     * @param base the base class.
     * @return a Map containing all matches.
     */
    public <ClassT> Map<String, Class<ClassT>> forType(final Class<ClassT> base) {
        return this.entries()
                .filter(entry -> entry.isType(base))
                .collect(Collectors.toMap(entry -> entry.getKey(), entry -> (Class<ClassT>) entry.getValue()));
    }
}
