/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.pkgmgr;

import com.runouw.hypercodec.JSONDecoder;
import com.runouw.hypercodec.JSONEncoder;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.StandardOpenOption;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * RPackageManager is designed to manage at least one instance of RPackage with
 * packages defined as dynamically loaded code or resources of which the code
 * may be dynamically compiled. Each package may define dependencies and
 * exports. All of the code for each package is kept on the same sandboxed
 * classpath. Resource compilation, loading, and management is delegated to the
 * implementation.
 *
 * @author zmichaels
 */
public final class RPackageManager {

    private static final ExecutorService ASYNC_WORKER = Executors.newCachedThreadPool();

    private static final Logger LOGGER = LoggerFactory.getLogger(RPackageManager.class);
    private final Path cacheDir;
    private final List<Path> srcDirs;
    private final Semaphore pkgLock = new Semaphore(1);
    private final Map<String, RPackageData> packages = new HashMap<>();
    private final Map<String, String> packagesLatest = new HashMap<>();
    private long timeOfCompile = 0;

    RPackageManager(final Path cacheDir, final List<Path> srcDirs) {
        this.cacheDir = cacheDir;
        this.srcDirs = srcDirs;
    }

    /**
     * Retrieves a Set of the fully qualified name of all packages that are
     * similar to the supplied package name. The intended use case is to
     * retrieve the set of all versions of a single registered package name.
     * However this method determines similar by comparing the beginnings of the
     * fully qualified names. Therefore it could be used to retrieve a set of
     * all packages that belong to a vendor or all packages that start with a
     * similar key phrase. This secondary feature is acknowledged, but not
     * guaranteed in future revisions.
     *
     * @param pkgName the package name.
     * @return
     */
    public Set<String> getRegisteredVersions(final String pkgName) {
        return Collections.unmodifiableSet(
                this.packages.keySet().stream()
                        .filter(name -> name.startsWith(pkgName))
                        .collect(Collectors.toSet()));
    }

    /**
     * Retrieves the fully qualified name for the most recent registered build
     * of a package.
     *
     * @param pkgName the name of the package.
     * @return the fully qualified name of the most recent build. May return
     * empty if no package matched the name.
     */
    public Optional<String> getLatestVersion(final String pkgName) {
        return Optional.ofNullable(this.packagesLatest.get(pkgName));
    }

    /**
     * Retrieves a Set of all registered package names. These do not include the
     * version numbers.
     *
     * @return an unmodifiable view of the registered package name set.
     */
    public Set<String> getPackageNameSet() {
        return Collections.unmodifiableSet(this.packagesLatest.keySet());
    }

    private static String entryToFullName(final Map.Entry<String, String> entry) {
        final String simpleName = entry.getKey();
        final String version = entry.getValue();

        return new StringBuilder(simpleName.length() + version.length() + 1)
                .append(simpleName)
                .append(':')
                .append(version)
                .toString();
    }

    /**
     * Retrieves the number of packages installed. Multiple versions of the same
     * package only count as one package.
     *
     * @return the number of packages.
     */
    public int size() {
        return this.packagesLatest.size();
    }

    /**
     * Retrieves a Stream of packages. Only the most recent edition of a package
     * is included.
     *
     * @return the stream of packages.
     */
    public Stream<RPackage> packages() {
        return this.packagesLatest.entrySet().stream()
                .map(RPackageManager::entryToFullName)
                .map(this.packages::get)
                .map(RPackage::new);
    }

    /**
     * Retrieves a Stream of all registered package names. These do not include
     * the version numbers.
     *
     * @return the Stream of package names.
     */
    public Stream<String> packageNames() {
        return this.packagesLatest.keySet().stream();
    }

    /**
     * Checks if the managed classes have been compiled.
     *
     * @return Returns true if either [code]scan[/code] or [code]compile[/code]
     * succeed.
     */
    public boolean isCompiled() {
        return this.timeOfCompile != 0L;
    }

    /**
     * Retrieves the most recent version of a package by name. The name must be
     * the package simple name. (vendor:package)
     *
     * @param simplPkgName the name of the package to select.
     * @return the package. May return empty if no package exists with the given
     * name.
     */
    public Optional<RPackage> getPackage(final String simplPkgName) {
        final String latestVersion = this.packagesLatest.get(simplPkgName);

        if (latestVersion == null) {
            return Optional.empty();
        } else {
            return this.getPackage(simplPkgName, latestVersion);
        }
    }

    /**
     * Retrieves the specific version of a package. The name must be in the
     * simple package name format (vendor:package)
     *
     * @param simplePkgName the name of the package to select.
     * @param version the version of the package.
     * @return the package. May return empty if no package exists with the
     * specified name and version.
     */
    public Optional<RPackage> getPackage(final String simplePkgName, final String version) {
        final String fullName = RPackageData.fullName(simplePkgName, version);
        final RPackageData pkgData = this.packages.get(fullName);

        if (pkgData == null) {
            return Optional.empty();
        } else {
            return Optional.of(new RPackage(pkgData));
        }
    }

    void registerPackage(final RPackageData pkgData) {
        try {
            this.pkgLock.acquire();

            this.packages.put(pkgData.getFullName(), pkgData);

            final String pkgName = pkgData.getSimpleName();
            final String newVersion = pkgData.getVersion();
            final String oldVersion = this.packagesLatest.put(pkgName, newVersion);

            if (oldVersion != null) {
                final int diff = RPackageData.compareVersion(newVersion, oldVersion);

                if (diff < 0) {
                    this.packagesLatest.put(pkgName, newVersion);
                }
            }
        } catch (InterruptedException ex) {
            throw new RuntimeException("Interrupted while registering package!", ex);
        } finally {
            this.pkgLock.release();
        }
    }

    RPackageData getResolvedPackageData(final String fullName) {
        while (true) {
            try {
                this.pkgLock.acquire();

                final RPackageData pkg = this.packages.get(fullName);

                if (pkg != null) {
                    return pkg;
                }
            } catch (InterruptedException ex) {
                LOGGER.warn("Package resolution was interrupted for package: [{}]!", fullName);
                LOGGER.debug(ex.getMessage(), ex);
                return null;
            } finally {
                this.pkgLock.release();
            }
        }
    }

    private boolean isZip(final Path path) {
        final String filename = path.getFileName().toString();

        return filename.endsWith(".zip");
    }

    private boolean isJar(final Path path) {
        final String filename = path.getFileName().toString();

        return filename.endsWith(".jar");
    }

    private static Future<?> tryDelete(final Path path) {
        final Runnable task = () -> {
            while (Files.exists(path)) {
                try {
                    Files.walkFileTree(path, new SimpleFileVisitor<Path>() {
                        @Override
                        public FileVisitResult visitFile(final Path file, final BasicFileAttributes attr) throws IOException {
                            Files.delete(file);

                            return FileVisitResult.CONTINUE;
                        }

                        @Override
                        public FileVisitResult postVisitDirectory(final Path directory, final IOException exc) throws IOException {
                            if (exc != null) {
                                throw exc;
                            }

                            Files.delete(directory);

                            return FileVisitResult.CONTINUE;
                        }
                    });

                    Files.deleteIfExists(path);
                } catch (IOException ex1) {
                    LOGGER.error("Unable to delete path: {}! Retrying...");
                    LOGGER.debug(ex1.getMessage(), ex1);

                    try {
                        Thread.sleep(10);
                    } catch (InterruptedException ex2) {
                        LOGGER.error("Interrupted delete({}) task!", path);
                        LOGGER.debug(ex2.getMessage(), ex2);
                        Thread.currentThread().interrupt();
                    }
                }
            }
        };

        return ASYNC_WORKER.submit(task);
    }

    private Path tmpJarDir;

    private Path extractJar(final Path path) throws IOException {
        if (this.tmpJarDir == null) {
            this.tmpJarDir = Files.createTempDirectory("com.runouw.pkgmgr");
        }

        final Path tmp = this.tmpJarDir.resolve(path.getFileName());
        final Path pkg = tmp.resolve("pkg");

        Files.createDirectories(pkg);
        LOGGER.trace("Extracting: {} to {}", path, pkg);

        try (ZipInputStream zis = new ZipInputStream(Files.newInputStream(path))) {
            for (ZipEntry entry = zis.getNextEntry(); entry != null; entry = zis.getNextEntry()) {
                final Path out = pkg.resolve(entry.getName());

                if (entry.isDirectory()) {
                    Files.createDirectory(out);
                } else {
                    Files.copy(zis, out);
                }
            }

            tmp.toFile().deleteOnExit();
        } catch (IOException ex) {
            LOGGER.error("Unable to extract jar [{}] to [{}]!", path, tmp);
            LOGGER.debug(ex.getMessage(), ex);
            Files.deleteIfExists(tmp);
        }

        return tmp;
    }

    private Path extractZip(final Path path) throws IOException {
        if (this.tmpJarDir == null) {
            this.tmpJarDir = Files.createTempDirectory("com.runouw.pkgmgr");
        }

        final Path tmp = this.tmpJarDir.resolve(path.getFileName());

        Files.createDirectory(tmp);
        LOGGER.trace("Extracting: {} to {}", path, tmp);

        try (ZipInputStream zis = new ZipInputStream(Files.newInputStream(path))) {
            for (ZipEntry entry = zis.getNextEntry(); entry != null; entry = zis.getNextEntry()) {
                final Path out = tmp.resolve(entry.getName());

                if (entry.isDirectory()) {
                    Files.createDirectory(out);
                } else {
                    Files.copy(zis, out);
                }
            }

            tmp.toFile().deleteOnExit();
        } catch (IOException ex) {
            LOGGER.error("Unable to extract jar [{}] to [{}]!", path, tmp);
            LOGGER.debug(ex.getMessage(), ex);
            Files.deleteIfExists(tmp);
        }

        return tmp;
    }

    /**
     * Compiles the source package. This is only valid if the package exists in
     * "source" form. This will (in order of operation) resolve dependencies,
     * copy binaries into cache directory, compile source files, and finally
     * execute the onInstall script.
     */
    public void compile() {
        if (this.srcDirs == null) {
            throw new UnsupportedOperationException("compile is not available on compiled-only instances of RPackageManager!");
        }
        try {
            if (!Files.exists(this.cacheDir)) {
                Files.createDirectories(this.cacheDir);
            }
        } catch (IOException ex) {
            LOGGER.error("Unable to create package cache: [{}]!", this.cacheDir);
            LOGGER.debug(ex.getMessage(), ex);
            return;
        }

        final List<RPackageData> pkgs;
        final ExceptionRedirector exred = new ExceptionRedirector(ex -> {
            LOGGER.error("Error while processing source dir(s)!");
            LOGGER.debug(ex.getMessage(), ex);
        });

        // find all jars; extract contents to temp folder
        final Stream<Path> jarDirs = this.srcDirs.stream()
                .flatMap(path -> exred.apply(Files::list, path))
                .filter(path -> Files.isRegularFile(path))
                .filter(this::isJar)
                .map(jar -> exred.apply(this::extractJar, jar))
                .filter(Objects::nonNull);

        final Stream<Path> zipDirs = this.srcDirs.stream()
                .flatMap(path -> exred.apply(Files::list, path))
                .filter(path -> Files.isRegularFile(path))
                .filter(this::isZip)
                .map(zip -> exred.apply(this::extractZip, zip))
                .filter(Objects::nonNull);

        final Stream<Path> archives = Stream.concat(jarDirs, zipDirs);

        // combine jar dirs with srcDirs; do not reprocess jars
        pkgs = Stream.concat(archives, this.srcDirs.stream())
                .flatMap(path -> exred.apply(Files::list, path))
                .filter(Objects::nonNull)
                .filter(path -> Files.isDirectory(path))
                .map(src -> new RPackageData(this.cacheDir, src))
                .collect(Collectors.toList());

        final int pkgCount = pkgs.size();
        final ExecutorService pool = Executors.newWorkStealingPool(pkgCount);
        final Queue<Future<?>> tasks = new ArrayDeque<>(pkgCount);

        //NOTE: this is way more confusing as a lambda
        for (RPackageData pkg : pkgs) {
            final Future<?> task = pool.submit(() -> {
                pkg.resolve(this);

                return pkg;
            });

            tasks.offer(task);
        }

        while (!tasks.isEmpty()) {
            final Future<?> task = tasks.poll();

            try {
                task.get(Constants.COMPILATION_TIMEOUT, TimeUnit.SECONDS);
            } catch (InterruptedException ex) {
                LOGGER.error("Package compilation was interrupted!");
                LOGGER.debug(ex.getMessage(), ex);
                pool.shutdownNow();
                return;
            } catch (ExecutionException ex) {
                LOGGER.error("Error compiling package!");
                LOGGER.trace(ex.getMessage(), ex);
            } catch (TimeoutException ex) {
                LOGGER.error("Package compilation timed out!");
                LOGGER.debug(ex.getMessage(), ex);
            }
        }

        this.timeOfCompile = System.currentTimeMillis();

        try {
            this.writeInfo();
        } catch (IOException ex) {
            LOGGER.error("Unable to write compiled plugin info file!");
            LOGGER.debug(ex.getMessage(), ex);
        } finally {
            tryDelete(this.tmpJarDir);
            this.tmpJarDir = null;
        }
    }

    private void writeInfo() throws IOException {
        final Path infoFile = this.cacheDir.resolve("info.json");

        Files.deleteIfExists(infoFile);

        final ByteBuffer out = ByteBuffer.allocate(1024 * 512); // 512KB

        try (FileChannel fch = FileChannel.open(infoFile, StandardOpenOption.CREATE_NEW, StandardOpenOption.WRITE)) {
            final Map<String, Object> infoStruct = new HashMap<>();

            infoStruct.put("latest", this.packagesLatest);
            infoStruct.put("timeOfCompile", this.timeOfCompile);

            final Map<String, Object> pkgStruct = new HashMap<>();

            this.packages.forEach((pkgName, pkg) -> {
                final Map<String, Object> meta = new HashMap<>();

                meta.put("vendor", pkg.getVendor());
                meta.put("name", pkg.getName());
                meta.put("version", pkg.getVersion());

                pkg.getDescription().ifPresent(desc -> meta.put("description", desc));

                final List<String> deps = pkg.listDependencies();

                if (!deps.isEmpty()) {
                    meta.put("depends-on", deps);
                }

                final Map<String, String> exports = pkg.getExportMap();

                if (!exports.isEmpty()) {
                    meta.put("export", exports);
                }

                meta.put("installedURL", pkg.getRawInstalledURL());
                pkg.getOnLoadScript().ifPresent(script -> meta.put("onLoad", script));

                pkgStruct.put(pkgName, meta);
            });

            infoStruct.put("packages", pkgStruct);

            final CharBuffer cout = out.asCharBuffer();

            JSONEncoder.encode(cout, infoStruct);
            cout.flip();

            fch.write(Constants.JSON_CHARSET.encode(cout));
        }
    }

    /**
     * Attempts to delete all cached packages. This will also clear any scanned
     * data.
     *
     * @return Future object that can be used to synchronize the clear task.
     * @throws IOException if something in the cached package directory cannot
     * be deleted.
     */
    public Future<?> clear() throws IOException {
        this.packages.clear();
        this.packagesLatest.clear();

        if (!Files.exists(this.cacheDir)) {
            LOGGER.trace("Cache dir doesn't exist; nothing to clear.");
            return null;
        }

        return tryDelete(this.cacheDir);
    }

    /**
     * Checks if any package needs to be recompiled.
     *
     * @return
     */
    public boolean needsRecompile() {
        if (this.srcDirs.stream()
                .filter(Files::exists)
                .count() == 0) {

            LOGGER.debug("No source directories exist!");
            return false;
        }

        final AtomicBoolean hasChanged = new AtomicBoolean(false);
        final Set<Future<?>> tasks = new HashSet<>();

        for (Path path : this.srcDirs) {
            final Future<?> task = ASYNC_WORKER.submit(() -> {

                Files.walkFileTree(path, new SimpleFileVisitor<Path>() {
                    @Override
                    public FileVisitResult visitFile(final Path file, final BasicFileAttributes attr) throws IOException {
                        if (testFileTime(file)) {
                            hasChanged.set(true);
                            return FileVisitResult.TERMINATE;
                        }

                        return FileVisitResult.CONTINUE;
                    }
                });

                return null;
            });
            
            tasks.add(task);
        }

        
        while (!tasks.isEmpty()) {
            tasks.removeIf(Future::isDone);
            
            if (hasChanged.get()) {
                tasks.forEach(task -> task.cancel(true));
                return true;
            }
        }
        
        return false;
    }

    private boolean testFileTime(final Path path) throws IOException {
        final FileTime time = Files.getLastModifiedTime(path);

        return time.toMillis() > this.timeOfCompile;
    }

    /**
     * Scans the package cache and restores the package manager's state.
     *
     * @throws IOException if the cache is corrupted or missing.
     */
    public void scan() throws IOException {
        final Path info = this.cacheDir.resolve("info.json");
        final Map<String, Object> infoStruct;
        
        try (FileChannel fch = FileChannel.open(info)) {
            final ByteBuffer tmp = ByteBuffer.allocate((int) fch.size());
            
            while (tmp.hasRemaining()) {
                fch.read(tmp);
            }
            
            tmp.flip();
            
            final CharBuffer json = Constants.JSON_CHARSET.decode(tmp);
            
            infoStruct = JSONDecoder.decode(json);
        }

        for (Map.Entry<String, Object> infoEntry : infoStruct.entrySet()) {
            switch (infoEntry.getKey()) {
                case "timeOfCompile":
                    this.timeOfCompile = ((Number) infoEntry.getValue()).longValue();
                    break;
                case "latest":
                    ((Map<String, Object>) infoEntry.getValue()).forEach((key, value) -> this.packagesLatest.put(key, value.toString()));
                    break;
                case "packages": {
                    final Map<String, Object> infoPackages = (Map<String, Object>) infoEntry.getValue();

                    infoPackages.keySet()
                            .stream()
                            .forEach(pkgName -> this.packages.put(pkgName, new RPackageData(this.cacheDir, null)));

                    final List<RPackageData> onLoadScripts = new ArrayList<>();

                    for (Map.Entry<String, Object> pkgEntry : infoPackages.entrySet()) {
                        final RPackageData pkgdata = this.packages.get(pkgEntry.getKey());
                        final Map<String, Object> infoPkg = (Map<String, Object>) pkgEntry.getValue();

                        for (Map.Entry<String, Object> infoPkgEntry : infoPkg.entrySet()) {
                            switch (infoPkgEntry.getKey()) {
                                case "name":
                                    pkgdata.setName(infoPkgEntry.getValue().toString());
                                    break;
                                case "vendor":
                                    pkgdata.setVendor(infoPkgEntry.getValue().toString());
                                    break;
                                case "version":
                                    pkgdata.setVersion(infoPkgEntry.getValue().toString());
                                    break;
                                case "description":
                                    pkgdata.setDescription(infoPkgEntry.getValue().toString());
                                    break;
                                case "installedURL":
                                    pkgdata.setInstalledURL(infoPkgEntry.getValue().toString());
                                    break;
                                case "depends-on":
                                    pkgdata.setDependencies(((List<String>) infoPkgEntry.getValue()).stream()
                                            .collect(Collectors.toMap(name -> name, this.packages::get)));
                                    break;
                                case "export":
                                    pkgdata.setExports(((Map<String, Object>) infoPkgEntry.getValue()).entrySet().stream()
                                            .collect(Collectors.toMap(pair -> pair.getKey(), pair -> pair.getValue().toString())));
                                    break;
                                case "onLoad":
                                    pkgdata.setOnLoadScript(infoPkgEntry.getValue().toString());
                                    onLoadScripts.add(pkgdata);
                                    break;
                                default:
                                    LOGGER.info("Unknown pair: {} = {}", infoPkgEntry.getKey(), infoPkgEntry.getValue());
                            }
                        }
                    }

                    for (RPackageData onLoadScript : new HashSet<>(onLoadScripts)) {
                        ASYNC_WORKER.submit(() -> onLoadScript.callOnLoad());
                    }
                }

                break;
                default:
                    LOGGER.info("Unknown pair: {} = {}", infoEntry.getKey(), infoEntry.getValue());
            }
        }
    }
}
