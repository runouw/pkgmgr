/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.pkgmgr;

/**
 *
 * @author zmichaels
 */
@FunctionalInterface
public interface InstallScript {
    /**
     * Callback that gets executed when a package is first installed.
     * @param pkg the package being installed.
     * @throws Exception if a resource failed to load.
     */
    void onInstall(RPackage pkg) throws Exception;
}
