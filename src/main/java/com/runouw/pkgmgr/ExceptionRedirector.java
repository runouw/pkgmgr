/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.pkgmgr;

import java.util.function.Consumer;

/**
 *
 * @author zmichaels
 */
final class ExceptionRedirector {

    static interface ExConsumer<T> {
        void accept(T value) throws Exception;
    }
    
    static interface ExRunnable {
        void run() throws Exception;
    }
    
    static interface ExFunction<P,R> {
        R apply(P value) throws Exception;
    }
    
    static interface ExPredicate<T> {
        boolean test(T t) throws Exception;
    }
    
    private final Consumer<Exception> handler;

    ExceptionRedirector(final Consumer<Exception> handler) {
        this.handler = handler;
    }
    
    void run(final ExRunnable task) {
        try {
            task.run();
        } catch (Exception ex) {
            this.handler.accept(ex);
        }
    }
    
    <T> void accept(final ExConsumer<T> c, final T value) {
        try {
            c.accept(value);
        } catch (Exception ex) {
            this.handler.accept(ex);            
        }
    }
    
    <T> boolean test(final ExPredicate<T> pd, final T test) {
        try {
            return pd.test(test);
        } catch (Exception ex) {
            this.handler.accept(ex);
            return false;
        }
    }
    
    <P, R> R apply(final ExFunction<P, R> fn, final P value) {
        try {
            return fn.apply(value);
        } catch (Exception ex) {
            this.handler.accept(ex);
            return null;
        }
    }
}
