/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.pkgmgr;

import com.runouw.hypercodec.JSONDecoder;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.StandardCopyOption;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.tools.Diagnostic;
import javax.tools.DiagnosticCollector;
import javax.tools.JavaCompiler;
import javax.tools.JavaCompiler.CompilationTask;
import javax.tools.JavaFileManager;
import javax.tools.JavaFileObject;
import javax.tools.SimpleJavaFileObject;
import javax.tools.ToolProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

final class RPackageData {

    static String simpleName(final String vendor, final String name) {
        return new StringBuilder(vendor.length() + name.length() + 1)
                .append(vendor)
                .append(':')
                .append(name)
                .toString();
    }

    static String fullName(final String vendor, final String name, final String version) {
        return new StringBuilder(vendor.length() + name.length() + version.length() + 2)
                .append(vendor)
                .append(':')
                .append(name)
                .append(':')
                .append(version)
                .toString();
    }

    static String fullName(final String simpleName, final String version) {
        return new StringBuilder(simpleName.length() + version.length() + 1)
                .append(simpleName)
                .append(':')
                .append(version)
                .toString();
    }

    static int compareVersion(final String v0, final String v1) {
        final int len = Math.min(v0.length(), v1.length());

        for (int i = 0; i < len; i++) {
            final int diff = v0.charAt(i) - v1.charAt(i);

            if (diff != 0) {
                return diff;
            }
        }

        return v0.length() - v1.length();
    }

    private static final Logger LOGGER = LoggerFactory.getLogger(RPackageData.class);
    private String srcPath;
    private String binPath;
    private String onLoad;
    private String description;
    private String simpleName;
    private String name;
    private String version;
    private String vendor;
    private String installedURL;
    private ClassLoader classLoader;
    private Map<String, String> exports;
    private final Path binDir;
    private final Path originDir;
    private Map<String, RPackageData> dependencies;
    private String fullName;
    private String onInstall;

    Stream<Map.Entry<String, String>> exports() {
        return this.exports.entrySet().stream();
    }

    Set<String> getExportedAliases() {
        return this.exports.keySet();
    }

    Optional<String> getExport(final String alias) {
        return Optional.ofNullable(this.exports.get(alias));
    }

    Collection<RPackageData> getDependentPackages() {
        return this.dependencies.values();
    }

    void setExports(final Map<String, String> exports) {
        this.exports = new HashMap<>(exports);
    }

    void setInstalledURL(final String installedURL) {
        this.installedURL = installedURL;
    }

    void setName(final String name) {
        this.name = name;
    }

    void setDescription(final String description) {
        this.description = description;
    }

    void setOnLoadScript(final String script) {
        this.onLoad = script;
    }

    void setVendor(final String vendor) {
        this.vendor = vendor;
    }

    void setVersion(final String version) {
        this.version = version;
    }

    void setDependencies(final Map<String, RPackageData> deps) {
        this.dependencies = new HashMap<>(deps);
    }

    RPackageData(final Path binDir, final Path originDir) {
        this.binDir = binDir;
        this.originDir = originDir;
    }

    Optional<String> getOnLoadScript() {
        return Optional.ofNullable(this.onLoad);
    }

    List<String> listDependencies() {
        if (this.dependencies == null || this.dependencies.isEmpty()) {
            return Collections.emptyList();
        } else {
            return this.dependencies.values()
                    .stream()
                    .map(RPackageData::getFullName)
                    .collect(Collectors.toList());
        }
    }

    Map<String, String> getExportMap() {
        if (this.exports == null || this.exports.isEmpty()) {
            return Collections.emptyMap();
        } else {
            return Collections.unmodifiableMap(this.exports);
        }
    }

    Optional<String> getDescription() {
        return Optional.ofNullable(this.description);
    }

    private Stream<RPackageData> deps() {
        final Stream<RPackageData> self = Stream.of(this);

        if (this.dependencies == null || this.dependencies.isEmpty()) {
            return self;
        } else {
            return Stream.concat(
                    self,
                    this.dependencies.values()
                            .stream()
                            .flatMap(RPackageData::deps));
        }
    }

    ClassLoader getClassLoader() {
        if (this.classLoader == null) {
            final Map<String, RPackageData> flatDeps = new HashMap<>();

            this.deps()
                    .distinct()
                    .forEach(dep -> {
                        final RPackageData oldDep = flatDeps.put(dep.getSimpleName(), dep);

                        if (oldDep != null) {
                            LOGGER.warn("Multiple versions of [{}] are referenced in package: [{}]!", oldDep.getSimpleName(), this.getFullName());

                            // restore the replaced dependency if it is newer.
                            if (compareVersion(oldDep.getVersion(), dep.getVersion()) > 0) {
                                flatDeps.put(oldDep.getSimpleName(), oldDep);
                            }
                        }
                    });

            final URL[] depURLs = new URL[flatDeps.size()];

            flatDeps.values().stream()
                    .map(RPackageData::getInstalledURL)
                    .filter(Optional::isPresent)
                    .map(Optional::get)
                    .collect(Collectors.toList())
                    .toArray(depURLs);

            this.classLoader = new URLClassLoader(depURLs);
        }

        return this.classLoader;
    }

    String getFullName() {
        if (this.fullName == null) {
            this.fullName = fullName(this.vendor, this.name, this.version);
        }

        return this.fullName;
    }

    String getName() {
        return this.name;
    }

    String getVendor() {
        return this.vendor;
    }

    String getVersion() {
        return this.version;
    }

    String getSimpleName() {
        if (this.simpleName == null) {
            this.simpleName = simpleName(this.vendor, this.name);
        }

        return this.simpleName;
    }

    Path getInstalledPath() {
        return this.binDir.resolve(Paths.get(this.vendor, this.name, this.version));
    }

    String getRawInstalledURL() {
        return this.installedURL;
    }

    Optional<URL> getInstalledURL() {
        try {
            return Optional.of(new URL(this.installedURL));
        } catch (IOException ex) {
            LOGGER.error("Unable to create URL from: [{}] in package: [{}]!", this.installedURL, this.getFullName());
            LOGGER.debug(ex.getMessage(), ex);
            return Optional.empty();
        }
    }

    void clear() {
        this.srcPath = null;
        this.binPath = null;
        this.classLoader = null;
        this.dependencies = null;
        this.description = null;
        this.exports = null;
        this.fullName = null;
        this.installedURL = null;
        this.name = null;
        this.onInstall = null;
        this.onLoad = null;
        this.simpleName = null;
        this.vendor = null;
        this.version = null;
    }

    private Stream<String> load() throws InfoNotFoundException, InvalidInfoException {
        if (!Files.exists(this.originDir)) {
            LOGGER.info("Package: [{}] does not have install information!");
            return Stream.empty();
        }

        final Path infoPath = this.originDir.resolve("info.json");

        if (!Files.exists(infoPath)) {
            LOGGER.debug("No info.json found on path: {}", this.originDir.toAbsolutePath());
            throw new InfoNotFoundException("No info.json on path: " + this.originDir.toAbsolutePath());
        }

        this.clear();

        final Map<String, Object> info;

        try {
            info = JSONDecoder.decode(infoPath, Constants.JSON_CHARSET);
        } catch (IOException ex) {
            LOGGER.debug("Invalid info.json at: {}", infoPath.toAbsolutePath());
            throw new InvalidInfoException("Invalid info.json at: " + infoPath.toAbsolutePath(), ex);
        }

        Stream<String> deps = null;

        for (Map.Entry<String, Object> entry : info.entrySet()) {

            switch (entry.getKey().toLowerCase()) {
                case "source-path":
                    this.srcPath = entry.getValue().toString();
                    break;
                case "binary-path":
                    this.binPath = entry.getValue().toString();
                    break;
                case "description":
                    this.description = entry.getValue().toString();
                    break;
                case "name":
                    this.name = entry.getValue().toString();
                    break;
                case "version":
                    this.version = entry.getValue().toString();
                    break;
                case "vendor":
                    this.vendor = entry.getValue().toString();
                    break;
                case "depends-on":
                    deps = ((List<Object>) entry.getValue()).stream()
                            .map(Object::toString);
                    break;
                case "export":
                    this.exports = new HashMap<>(((Map<String, Object>) entry.getValue())
                            .entrySet()
                            .stream()
                            .collect(Collectors.toMap(pair -> pair.getKey(), pair -> pair.getValue().toString())));
                    break;
                case "onload":
                    this.onLoad = entry.getValue().toString();
                    break;
                case "oninstall":
                    onInstall = entry.getValue().toString();
                    break;
                default:
                    LOGGER.info("Unrecognized token: {} = {}", entry.getKey(), entry.getValue());
                    break;
            }
        }

        final Path installedPath = this.getInstalledPath();
        final String protocol = "file://";
        final String absPath = installedPath.toAbsolutePath().toString();

        this.installedURL = new StringBuilder(protocol.length() + absPath.length() + 1)
                .append(protocol)
                .append(absPath)
                .append('/')
                .toString();

        if (!Files.exists(installedPath)) {
            try {
                Files.createDirectories(installedPath);
            } catch (IOException ex) {
                LOGGER.error("Unable to construct cache directory: [{}]!", installedPath);
                LOGGER.debug(ex.getMessage(), ex);
                return Stream.empty();
            }
        }

        return (deps != null) ? deps : Stream.empty();
    }

    private void copyBinaries() {
        if (this.binPath == null) {
            return;
        }

        final Path binPath = this.originDir.resolve(this.binPath);

        LOGGER.trace("Copying binaries from: {}", binPath);

        if (!Files.exists(binPath)) {
            return;
        }

        final Path installPath = this.getInstalledPath();

        try {
            Files.walkFileTree(binPath, new SimpleFileVisitor<Path> () {
                @Override
                public FileVisitResult preVisitDirectory(final Path dir, BasicFileAttributes attr) {
                    final Path dst = installPath.resolve(binPath.relativize(dir));
                    
                    try {
                        Files.createDirectories(dst);
                    } catch (IOException ex) {
                        LOGGER.error("Unable to create directory: {}", dst);
                        LOGGER.debug(ex.getMessage(), ex);
                        return FileVisitResult.SKIP_SUBTREE;
                    }
                    
                    return FileVisitResult.CONTINUE;
                }
                
                @Override
                public FileVisitResult visitFile(final Path file, BasicFileAttributes attr) {
                    final Path relPath = binPath.relativize(file);
                    final Path outPath = installPath.resolve(relPath);
                    
                    try {
                        Files.copy(file, outPath);
                    } catch (IOException ex) {
                        LOGGER.error("Unable to copy file: {} to {}!", file, outPath);
                        LOGGER.debug(ex.getMessage(), ex);
                    }
                    
                    return FileVisitResult.CONTINUE;
                }
            });
        } catch (IOException ex) {
            LOGGER.error("Error while traversing directory: {}!", binPath);
            LOGGER.debug(ex.getMessage(), ex);            
        }
    }

    private static JavaFileObject newJavaSrcObject(final Path path) {
        return new SimpleJavaFileObject(path.toUri(), JavaFileObject.Kind.SOURCE) {
            @Override
            public CharSequence getCharContent(boolean ignoreEncodingErrors) throws IOException {
                return new String(Files.readAllBytes(path));
            }
        };
    }
    
    private static List<JavaFileObject> getSources(final Path srcDir) throws IOException {
        final List<JavaFileObject> out = new ArrayList<>();
        
        Files.walkFileTree(srcDir, new SimpleFileVisitor<Path>() {
            @Override
            public FileVisitResult visitFile(final Path file, final BasicFileAttributes attr) {
                if (file.toString().endsWith(".java")) {
                    out.add(newJavaSrcObject(file));
                }
                
                return FileVisitResult.CONTINUE;
            }
        });
        
        return out;
    }

    private String getClassPath() {
        final StringBuilder classPath = new StringBuilder(4096);

        classPath.append(System.getProperty("java.class.path"));

        this.dependencies.values().stream()
                .map(RPackageData::getInstalledPath)
                .map(Path::toAbsolutePath)
                .forEach(p -> classPath.append(Constants.CLASSPATH_SEP).append(p));

        return classPath.toString();
    }

    private void compile() {
        if (this.srcPath == null) {
            return;
        }

        final Path srcPath = this.originDir.resolve(this.srcPath);

        if (!Files.exists(srcPath)) {
            return;
        }

        final List<JavaFileObject> srcs;

        try {
            srcs = getSources(srcPath);
        } catch (IOException ex) {
            LOGGER.error("Unable to collect all sources in path: [{}]!", srcPath);
            LOGGER.debug(ex.getMessage(), ex);
            return;
        }

        final Path outputDir = this.getInstalledPath();
        final String classpath = this.getClassPath();
        final JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
        final List<String> options = Arrays.asList("-d", outputDir.toAbsolutePath().toString(), "-classpath", classpath, "-Xlint:all");
        final DiagnosticCollector<JavaFileObject> diagnostics = new DiagnosticCollector<>();
        final JavaFileManager fileManager = compiler.getStandardFileManager(diagnostics, null, null);
        final CompilationTask task = compiler.getTask(null, fileManager, diagnostics, options, null, srcs);

        task.call();

        for (Diagnostic<? extends JavaFileObject> d : diagnostics.getDiagnostics()) {
            switch (d.getKind()) {
                case ERROR:
                    LOGGER.error("{}", d);
                    break;
                case WARNING:
                case MANDATORY_WARNING:
                    LOGGER.warn("{}", d);
                    break;
                default:
                    LOGGER.debug("{}", d);
                    break;
            }
        }
    }

    void resolve(final RPackageManager pkgmgr) throws PluginResolutionException {
        try {
            this.dependencies = this.load()
                    .map(pkgmgr::getResolvedPackageData)
                    .collect(Collectors.toMap(RPackageData::getSimpleName, self -> self));
        } catch (InfoNotFoundException | InvalidInfoException ex) {
            throw new PluginResolutionException(ex);
        }

        this.copyBinaries();
        this.compile();

        pkgmgr.registerPackage(this);

        if (this.onInstall != null) {
            this.callOnInstall();
        }

        if (this.onLoad != null) {
            this.callOnLoad();
        }
    }

    void callOnLoad() {
        try {
            final Class<?> loadScriptClass = Class.forName(this.onLoad, true, this.getClassLoader());
            final LoadScript script = (LoadScript) loadScriptClass.newInstance();

            script.onLoad(new RPackage(this));
        } catch (ClassNotFoundException ex) {
            LOGGER.error("Could not find load script: [{}] for package: [{}]!", this.onInstall, this.getFullName());
            LOGGER.debug(ex.getMessage(), ex);
        } catch (InstantiationException ex) {
            LOGGER.error("Unable to call constructor for load script: [{}] in package: [{}]!", this.onInstall, this.getFullName());
            LOGGER.debug(ex.getMessage(), ex);
        } catch (IllegalAccessException ex) {
            LOGGER.error("Unable to access load script: [{}] for package: [{}]!", this.onInstall, this.getFullName());
            LOGGER.debug(ex.getMessage(), ex);
        } catch (Exception ex) {
            LOGGER.error("Error executing load script: [{}] for package: [{}]!", this.onInstall, this.getFullName());
            LOGGER.debug(ex.getMessage(), ex);
        }
    }

    private void callOnInstall() {
        try {
            final Class<?> installScriptClass = Class.forName(this.onInstall, true, this.getClassLoader());
            final InstallScript script = (InstallScript) installScriptClass.newInstance();

            script.onInstall(new RPackage(this));
        } catch (ClassNotFoundException ex) {
            LOGGER.error("Could not find install script: [{}] for package: [{}]!", this.onInstall, this.getFullName());
            LOGGER.debug(ex.getMessage(), ex);
        } catch (InstantiationException ex) {
            LOGGER.error("Unable to call constructor for install script: [{}] in package: [{}]!", this.onInstall, this.getFullName());
            LOGGER.debug(ex.getMessage(), ex);
        } catch (IllegalAccessException ex) {
            LOGGER.error("Unable to access install script: [{}] for package: [{}]!", this.onInstall, this.getFullName());
            LOGGER.debug(ex.getMessage(), ex);
        } catch (Exception ex) {
            LOGGER.error("Error executing install script: [{}] for package: [{}]!", this.onInstall, this.getFullName());
            LOGGER.debug(ex.getMessage(), ex);
        }
    }

    @Override
    public String toString() {
        return this.getFullName();
    }

    @Override
    public boolean equals(final Object other) {
        if (other == null) {
            return false;
        } else if (other instanceof RPackageData) {
            final RPackageData otherPkg = (RPackageData) other;

            return this.getFullName().equals(otherPkg.getFullName());
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return this.getFullName().hashCode();
    }

    long getModificationTime() {
        try {
            final AtomicLong modifyTime = new AtomicLong(0);

            Files.walkFileTree(this.originDir, new SimpleFileVisitor<Path>() {
                @Override
                public FileVisitResult visitFile(Path file, final BasicFileAttributes attr) {
                    try {
                        final FileTime time = Files.getLastModifiedTime(file);
                        final long millis = time.toMillis();
                        
                        if (time.toMillis() > modifyTime.get()) {
                            modifyTime.set(millis);
                        }
                    } catch (IOException ex) {
                        LOGGER.error("Unable to get file modification for file: {} in package: {}!", file, RPackageData.this.getFullName());
                        LOGGER.debug(ex.getMessage(), ex);
                    }
                    
                    return FileVisitResult.CONTINUE;
                }
            });

            return modifyTime.get();
        } catch (IOException ex) {
            LOGGER.error("Unable to walk source directory: [{}] for package: [{}]!", this.originDir, this.getFullName());
            LOGGER.debug(ex.getMessage(), ex);
            return System.currentTimeMillis();
        }
    }
}
