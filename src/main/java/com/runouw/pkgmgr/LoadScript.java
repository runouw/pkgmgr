/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.pkgmgr;

/**
 *
 * @author zmichaels
 */
@FunctionalInterface
public interface LoadScript {
    void onLoad(RPackage pkg) throws Exception;
}
