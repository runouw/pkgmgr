/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.runouw.pkgmgr;

import java.nio.charset.Charset;
import java.nio.charset.IllegalCharsetNameException;
import java.nio.charset.StandardCharsets;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

final class Constants {
    static final long COMPILATION_TIMEOUT = Long.getLong("com.runouw.pkgmgr.constants.compilation_timeout", 10);
    static final Charset JSON_CHARSET;
    static final String CLASSPATH_SEP;        
    private static final Logger LOGGER =LoggerFactory.getLogger(Constants.class);
    
    static {
        final String jsonCharsetName = System.getProperty("com.runouw.pkgmgr.constants.json_charset", "UTF8");
        Charset jsonCharset;
        
        try {
            jsonCharset = Charset.forName(jsonCharsetName);
        } catch (IllegalCharsetNameException ex) {
            LOGGER.error("Illegal charset name: [{}]!", jsonCharsetName);
            LOGGER.debug(ex.getMessage(), ex);
            jsonCharset = StandardCharsets.UTF_8;
        }
        
        JSON_CHARSET = jsonCharset;
        
        final String os = System.getProperty("os.name").toLowerCase();
        
        if (os.startsWith("win")) {
            CLASSPATH_SEP = ";";
        } else {
            CLASSPATH_SEP = ":";
        }                
    }
    
    private Constants() {}
}
