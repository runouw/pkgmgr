package animals.barn;

import animals.Animal;

public class Cow extends Animal {
    @Override
    public String say() {
        return "Meow";
    }
}
