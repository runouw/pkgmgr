package animals.house;

import animals.Animal;

public class Cat extends Animal {
    @Override
    public String say() {
        return "Meow";
    }
}
